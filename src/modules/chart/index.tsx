import { Graph, Cell, CellView, Node, Rectangle } from "@antv/x6";
import { useEffect, useRef } from "react";
import { DATA } from "src/data/swimlane";

Graph.registerNode(
  "lane",
  {
    inherit: "rect",
    markup: [
      {
        tagName: "rect",
        selector: "body",
      },
      {
        tagName: "rect",
        selector: "name-rect",
      },
      {
        tagName: "text",
        selector: "name-text",
      },
    ],
    attrs: {
      body: {
        fill: "#FFF",
        stroke: "#5F95FF",
        strokeWidth: 1,
      },
      "name-rect": {
        width: 200,
        height: 30,
        fill: "#5F95FF",
        stroke: "#fff",
        strokeWidth: 1,
        x: -1,
      },
      "name-text": {
        ref: "name-rect",
        refY: 0.5,
        refX: 0.5,
        textAnchor: "middle",
        fontWeight: "bold",
        fill: "#fff",
        fontSize: 12,
      },
    },
  },
  true
);

Graph.registerNode(
  "hz-line",
  {
    inherit: "rect",
    markup: [
      {
        tagName: "rect",
        selector: "body",
      },
      {
        tagName: "rect",
        selector: "name-rect",
      },
    ],
    attrs: {
      body: {
        fill: "transparent",
        stroke: "#5F95FF",
        strokeWidth: 1,
      },
    },
  },
  true
);

Graph.registerNode(
  "lane-rect",
  {
    inherit: "rect",
    width: 100,
    height: 60,
    attrs: {
      body: {
        strokeWidth: 1,
        stroke: "#5F95FF",
        fill: "#EFF4FF",
      },
      text: {
        fontSize: 12,
        fill: "#262626",
      },
    },
  },
  true
);

Graph.registerNode(
  "lane-polygon",
  {
    inherit: "polygon",
    width: 80,
    height: 80,
    attrs: {
      body: {
        strokeWidth: 1,
        stroke: "#5F95FF",
        fill: "#EFF4FF",
        refPoints: "0,10 10,0 20,10 10,20",
      },
      text: {
        fontSize: 12,
        fill: "#262626",
      },
    },
  },
  true
);

Graph.registerEdge(
  "lane-edge",
  {
    inherit: "edge",
    attrs: {
      line: {
        stroke: "#A2B1C3",
        strokeWidth: 2,
      },
    },
    label: {
      attrs: {
        label: {
          fill: "#A2B1C3",
          fontSize: 12,
        },
      },
    },
  },
  true
);


const Chart = () => {
  const graph = useRef<Graph | null>(null);

  const getHorizontalRect = (g: Graph, id?: string) => {
    if (!id) {
      return undefined
    }

    const rect = g.getCellById(id) as Node || undefined

    return rect ? rect.getBBox() : undefined;
  }

  useEffect(() => {
    const graphInstance = new Graph({
      container: document.getElementById("container")!,
      connecting: {
        router: "orth",
      },
      translating: {
        restrict: (cellView: CellView) => {
          const cell = cellView.cell as Node;
          const parentId = cell.prop("parent");

          if (parentId) {
            const parentNode = graphInstance.getCellById(parentId) as Node;
            const hzNode = getHorizontalRect(graphInstance, cell.data?.horizontalLine)
            if (parentNode && hzNode) {
              return new Rectangle(parentNode.getBBox().x, hzNode.y, parentNode.getBBox().width, hzNode.height)
            }
          }
          return cell.getBBox();
        },
      },
    });

    const cells: Cell[] = [];
    DATA.forEach((item: any) => {
      if (item.shape === "lane-edge") {
        cells.push(graphInstance.createEdge(item));
      } else {
        const node = graphInstance.createNode(item);
        cells.push(node);
      }
    });

    graphInstance.resetCells(cells);
    graphInstance.zoomToFit({ padding: 10, maxScale: 1 });
    graph.current = graphInstance;
  }, []);

  return (
    <div
      id="container"
      style={{ width: window.innerWidth, height: window.innerHeight }}
    ></div>
  );
};

export default Chart;
